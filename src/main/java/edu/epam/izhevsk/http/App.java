package edu.epam.izhevsk.http;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.charset.Charset;

public class App {

	private static final String YANDEX_SCHEME = "https";
	private static final String YANDEX_HOST = "yandex.ru";
	private static final String YANDEX_SEARCH_PATH = "/search/";
	private static final String YANDEX_SEARCH_TEXT_PARAM = "text";

	private static String searchQuery = "test";

	public static void main(String[] args) {

		if (args.length > 0) {
			searchQuery = args[0];
		}

		URIBuilder uriBuilder = new URIBuilder();

		uriBuilder.setScheme(YANDEX_SCHEME);
		uriBuilder.setHost(YANDEX_HOST);
		uriBuilder.setPath(YANDEX_SEARCH_PATH);
		uriBuilder.addParameter(YANDEX_SEARCH_TEXT_PARAM, searchQuery);

		try {
			System.out.println(uriBuilder.build().toString());
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}

		CloseableHttpClient httpClient = HttpClients.createDefault();


		CloseableHttpResponse response = null;

		try {

			HttpGet httpGet = new HttpGet(uriBuilder.build());

			response = httpClient.execute(httpGet);

			System.out.println(response.getStatusLine());

			for (Header header : response.getAllHeaders()) {
				System.out.println(header.getName() + ": " + header.getValue());
			}

			HttpEntity entity = response.getEntity();

			try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(entity.getContent(), Charset.forName("UTF-8")))) {
				for (int i = 0; i < 30; ++i) {
					String line = bufferedReader.readLine();
					System.out.println(i + ": " + line);
				}
			}

			EntityUtils.consume(entity);

		} catch (IOException | URISyntaxException e) {
			e.printStackTrace();
		} finally {
			try {
				if (response != null) {
					response.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}


	}

}
